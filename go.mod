module purelb.io

go 1.12

require (
	github.com/go-kit/kit v0.9.0
	github.com/go-resty/resty/v2 v2.3.0
	github.com/google/go-cmp v0.5.2
	github.com/googleapis/gnostic v0.2.0 // indirect
	github.com/hashicorp/memberlist v0.2.2
	github.com/imdario/mergo v0.3.6 // indirect
	github.com/prometheus/client_golang v1.0.0
	github.com/stretchr/testify v1.4.0
	github.com/vishvananda/netlink v1.1.0
	k8s.io/api v0.18.8
	k8s.io/apimachinery v0.18.8
	k8s.io/client-go v0.18.8
	k8s.io/code-generator v0.18.8
	k8s.io/klog v1.0.0
)
